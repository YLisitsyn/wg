
#include <stdio.h>

void* myMemcpy(char* dst, const char* src, int nBytes) {
	while (nBytes--)
		*dst++ = *src++;
	return dst;
}