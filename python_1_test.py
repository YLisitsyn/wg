import unittest
from copy import deepcopy
from python_1 import dict_diff


class DiffTest(unittest.TestCase):

    def test_1(self):
        data = {
            'a': {
                'a1': 1,
                'a2': 2,
            },
            'b': {
                'b1': {
                    'b11': 1,
                    'b12': 2,
                },
            }
        }
        start_data = deepcopy(data)

        data['a']['a1'] = 3
        data['b']['b1']['b11'] = 5
        res = {'a': {'a1': 3}, 'b': {'b1': {'b11': 5}}}
        self.assertEqual(dict_diff(start_data, data), res)

    def test_2(self):
        data = {
            'a': {
                'a1': 1,
                'a2': 2,
            },
            'b': {
                'b1': {
                    'b11': 1,
                    'b12': 2,
                },
            }
        }
        start_data = deepcopy(data)
        data['c'] = 10
        res = {'c': 10}
        self.assertEqual(dict_diff(start_data, data), res)

    def test_3(self):
        data = {
            'a': {
                'a1': 1,
                'a2': 2,
            },
            'b': {
                'b1': {
                    'b11': 1,
                    'b12': 2,
                },
            }
        }
        start_data = deepcopy(data)
        del data['b']
        res = {'b': None}
        self.assertEqual(dict_diff(start_data, data), res)

    def test_4(self):
        data = {
            'a': {
                'a1': 1,
                'a2': 2,
            },
            'b': {
                'b1': {
                    'b11': 1,
                    'b12': 2,
                },
            }
        }
        start_data = deepcopy(data)
        del data['b']
        d = {'c': {'c1': {'c11': {'c111': 10}}}}
        data.update(d)
        res = {'b': None, 'c': {'c1': {'c11': {'c111': 10}}}}
        self.assertEqual(dict_diff(start_data, data), res)

    def test_5(self):
        data = {
            'a': {
                'a1': 1,
                'a2': 2,
            },
            'b': {
                'b1': {
                    'b11': 1,
                    'b12': 2,
                },
            }
        }
        res = {'a': None, 'b': None}
        self.assertEqual(dict_diff(data, {}), res)

    def test_6(self):
        data = {
            'a': {
                'a1': 1,
                'a2': 2,
            },
            'b': {
                'b1': {
                    'b11': 1,
                    'b12': 2,
                },
            }
        }
        res = data
        self.assertEqual(dict_diff({}, data), res)

    def test_7(self):
        data = {
            'a': {
                'a1': 1,
                'a2': 2,
            },
            'b': {
                'b1': {
                    'b11': 1,
                    'b12': 2,
                },
            }
        }
        res = data
        self.assertEqual(dict_diff("213123123", data), res)

    def test_8(self):
        data = {
            'a': {
                'a1': 1,
                'a2': 2,
            },
            'b': {
                'b1': {
                    'b11': 1,
                    'b12': 2,
                },
            }
        }
        self.assertEqual(dict_diff(data, "123123213"), "123123213")


if __name__ == '__main__':
    unittest.main()
