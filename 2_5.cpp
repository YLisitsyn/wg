
Direction testWayPoint(const Vector3& dronePosition, const Vector3& droneForward, const Vector3& targetWaypoint, const Vector3& worldUp ) {

    Vector3 dirvec = NULL;
    subtract(targetWaypoint, dronePosition, dirvec);

    if (length(dirvec) < 10.0f) 
		return REJECTED;

    normalize(dirvec);

    Vector3 dirdiff = NULL;
    subtract(dirvec, droneForward, dirdiff);

    if (length(dirdiff) > 1) 
		return REJECTED; 
 
    Vector3 sidedir = NULL;
    cross(worldUp, droneForward, sidedir);
 
    if (dot(sidedir, dirvec) < 0)
        return LEFT;
    else 
        return RIGHT;
    
}