#include <iostream>
#include <stdio.h>

using namespace std;

inline bool connected(pair<short, short> p, int first, int second) {

    bool first_condition = (p.first == first && p.second == second);
    bool second_condition = (p.first == second && p.second == first);
    return first_condition || second_condition;
}

void check_connectivity(short* indexTriples, int* connectivityOut, int vertices, pair<short, short> p, int index, int shift) {

    if(p.first > p.second) {
        swap(p.first, p.second);
    }

    for(int j = 0; j < vertices; j += 3) {

        if(j == index) {
            continue;
        }

        connectivityOut[index + shift] = -1;

        int first = indexTriples[j];
        int second = indexTriples[j + 1];
        int third = indexTriples[j + 2];

        if(connected(p, first, second)) {
            connectivityOut[index + shift] = j;
            break;
        }
        if(connected(p, second, third)) {
            connectivityOut[index + shift] = j + 1;
            break;
        }
        if(connected(p, third, first)) {
            connectivityOut[index + shift] = j + 2;
        }
    }
}

void findConnectivity(short* indexTriples, int T, int* connectivityOut) {

    int vertices = 3 * T;

    for(int i = 0; i < vertices; i += 3) {

        pair<short, short> p1 = make_pair(indexTriples[i], indexTriples[i + 1]);
        pair<short, short> p2 = make_pair(indexTriples[i + 1], indexTriples[i + 2]);
        pair<short, short> p3 = make_pair(indexTriples[i + 2], indexTriples[i]);

        check_connectivity(indexTriples, connectivityOut, vertices, p1, i, 0);
        check_connectivity(indexTriples, connectivityOut, vertices, p2, i, 1);
        check_connectivity(indexTriples, connectivityOut, vertices, p3, i, 2);
    }
}


int main()
{
    int T = 3;
    int* connectivityOut = new int[3 * T];
    short indices[] { 0,2,7,1,3,5,6,2,0 };

    findConnectivity(indices, 3, connectivityOut);

    for(int i = 0; i < 3 * T; ++i)
        printf("%i ", connectivityOut[i]);

}