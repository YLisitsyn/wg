

/*
 * Сначала программа записывается в память. Как только программа записалась в память, CPU начинает выполнять команды машинного кода. Компилятор уже определил начальную точку выполнения программы.
 * - extern int x - ищет в памяти глобальную переменную, которая была объявлена раньше.
 * - происходит переход в облать памяти где лежит void func().
 * - для переменной y определяется область памяти размером с int;
 * - значение y записывается в облость памяти
 * - для переменной y определяется область памяти размером с int;
 * - для переменной x = BSS
 * - переходим в область памяти, записываем туда значение x и адресс y
 *  -- Если inline int foo(), то перехода не проиходит
 * - Достается значение которое хранится по адрессу y и складывается со значением x
 * - результат записывается в переменную r
 * - функция printf выводит данные на экран

 */