
#include <stdio.h>

struct Node {
    struct Node* next;
    int val;
};

void show_node(Node *head) {

    while (head != NULL) {
        printf("%i,", head->val);
        head = head->next;
    }
}

void reverse(Node **head) {

    Node *prev, *preRes, *res = NULL;
    preRes = *head;
    while (preRes) {
        prev = preRes;
        preRes = preRes->next;
        prev->next = res;
        res = prev;
    }
    *head = res;
}


void reverseAfter(struct Node* head, int val) {

    while (head->next && head->next->val != val)
        head = head->next;
    reverse(&head->next);
}

int main() {

    Node* head = NULL;

    for (int i = 10; i >= 0; i--) {
        Node *n = new Node;
        n->next = head;
        n->val = i;
        head = n;
    }

    reverseAfter(head, 3);
    show_node(head);

    return 0;
}