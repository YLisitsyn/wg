#include <stdio.h>

int fac_r(int n){
    if(n == 0) return 1;
    return fac_r(n - 1) * n;
}

int fac_f(int n){

    int f = 1;

    for(int i = 1; i <= n; ++i){
        f *= i;
    }

    return  f;
}


int main()
{
    printf("Recursive = %i\n", fac_r(12));
    printf("For = %i", fac_f(12));
    return 0;
}

/* Вариант с циклом работыет быстрее:
 * 1) Меньшее количество операций
 * 2) Проход по циклу работает быстрее, чем переход в функцию
 * 3) Занимает меньше памяти
 */

