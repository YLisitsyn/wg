
def dict_diff(start_dict, change_dict):
    if not (isinstance(start_dict, dict) and isinstance(change_dict, dict)):
        return change_dict
    res = dict()
    for key in sorted(set(start_dict.keys() + change_dict.keys())):
        value_1 = start_dict.get(key, None)
        value_2 = change_dict.get(key, None)
        if value_1 != value_2:
            res[key] = dict_diff(value_1, value_2)
    return res

if __name__ == "__main__":

    from copy import deepcopy

    data = {
        'a': {
            'a1': 1,
            'a2': 2,
        },
        'b': {
            'b1': {
                'b11': 1,
                'b12': 2,
            },
        }
    }

    start_data = deepcopy(data)

    data['a']['a1'] = 3
    data['b']['b1']['b11'] = 5

    r = dict_diff(start_data, data)

    print r

